//
//  HTTPRequest.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation

// This file provides some boilerplate stuff we can use as the project expands.
// It's not strictly necessary for the code challenge, but will make things easier down the road.

public enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case delete  = "DELETE"
}

protocol HTTPRequest {
    associatedtype ReturnType: Decodable
    var path: String { get set }
    var method: HTTPMethod { get set }
    var headers: [String: String]? { get set }
    var queryParams: [String: String]? { get set }
    var body: [String: Any]? { get set }
}

extension HTTPRequest {

    func asURLRequest(baseURL: String) -> URLRequest? {
        var urlString = baseURL + path
        if let queryParams = queryParams {
            let parameterString = self.stringFromHttpParameters(params: queryParams)
            urlString = "\(urlString)?\(parameterString)"
        }
        guard let finalURL = URL(string: urlString) else { return nil }
        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue
        request.httpBody = requestBodyFrom(params: body)
        request.allHTTPHeaderFields = headers
        return request
    }

    private func requestBodyFrom(params: [String: Any]?) -> Data? {
        guard let params = params else { return nil }
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return nil
        }
        return httpBody
    }
    
    private func stringFromHttpParameters(params: [String: String]) -> String {
        let parameterArray = params.map { (key, value) -> String in
            guard let percentEscapedKey = key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                  let percentEscapedValue = value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                      return ""
                  }
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        return parameterArray.joined(separator: "&")
    }
}
