//
//  APIClient.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Combine
import Foundation

// This file provides some boilerplate stuff we can use as the project expands.
// It's not strictly necessary for the code challenge, but will make things easier down the road.

enum NetworkError: LocalizedError, Equatable {
    case invalidRequest
    case badRequest
    case unauthorized
    case forbidden
    case notFound
    case error4xx(_ code: Int)
    case serverError
    case error5xx(_ code: Int)
    case decodingError
    case urlSessionFailed(_ error: URLError)
    case unknownError
}

// The purpose of the APIClient struct is to have a centralized place to make network requests from.
struct APIClient {
    
    func dispatch<R: HTTPRequest>(_ request: R) -> AnyPublisher<R.ReturnType, NetworkError> {
        guard let urlRequest = request.asURLRequest(baseURL: Constants.basePath) else {
            return Fail(outputType: R.ReturnType.self, failure: NetworkError.invalidRequest)
                .eraseToAnyPublisher()
        }
        let requestPublisher: AnyPublisher<R.ReturnType, NetworkError> = self.handle(request: urlRequest)
        return requestPublisher.eraseToAnyPublisher()
    }

    private func handle<ReturnType: Decodable>(request: URLRequest) -> AnyPublisher<ReturnType, NetworkError> {
        return URLSession.shared
            .dataTaskPublisher(for: request)
        
            // Map on Request response
            .tryMap({ data, response in

                // If the response is invalid, throw an error
                if let response = response as? HTTPURLResponse,
                   !(200...299).contains(response.statusCode) {
                    throw httpError(response.statusCode)
                }
                return data
            })
        
            // Decode data using ReturnType
            .decode(type: ReturnType.self, decoder: JSONDecoder())
        
            // Handle any decoding errors
            .mapError { error in
                return handleError(error)
            }
            .eraseToAnyPublisher()
    }

    private func httpError(_ statusCode: Int) -> NetworkError {
        switch statusCode {
        case 400: return .badRequest
        case 401: return .unauthorized
        case 403: return .forbidden
        case 404: return .notFound
        case 402, 405...499: return .error4xx(statusCode)
        case 500: return .serverError
        case 501...599: return .error5xx(statusCode)
        default: return .unknownError
        }
    }

    private func handleError(_ error: Error) -> NetworkError {
        switch error {
        case is Swift.DecodingError:
            return .decodingError
        case let urlError as URLError:
            return .urlSessionFailed(urlError)
        default:
            return .unknownError
        }
    }
}
