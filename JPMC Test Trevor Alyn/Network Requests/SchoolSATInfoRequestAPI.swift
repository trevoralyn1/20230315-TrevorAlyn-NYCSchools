//
//  SchoolSATInfoRequestAPI.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation
import Combine

protocol SchoolSATInfoRequestAPIProtocol {
    static func getSATInfo(schoolID: String) -> AnyPublisher<[SchoolSATInfoModel], NetworkError>
}

struct SchoolSATInfoRequest: HTTPRequest {
    typealias ReturnType = [SchoolSATInfoModel]
    var path = JSONFileNames.satScores.rawValue
    var method: HTTPMethod = .get
    var headers: [String : String]? = ["X-App-Token" : Constants.appToken, "Content-Type" : "application/json"]
    var queryParams: [String : String]?
    var body: [String : Any]?
 
    init(schoolID: String) {
        self.queryParams = ["dbn" : schoolID]
    }
}

class SchoolSATInfoRequestAPI: SchoolSATInfoRequestAPIProtocol {

    static func getSATInfo(schoolID: String) -> AnyPublisher<[SchoolSATInfoModel], NetworkError> {
        return APIClient().dispatch(SchoolSATInfoRequest(schoolID: schoolID))
    }
}
