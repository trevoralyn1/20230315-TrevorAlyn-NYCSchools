//
//  SchoolsRequestAPI.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation
import Combine

protocol SchoolsRequestAPIProtocol {
    static func getSchools() -> AnyPublisher<[SchoolModel], NetworkError>
}

struct SchoolsRequest: HTTPRequest {
    typealias ReturnType = [SchoolModel]
    var path = JSONFileNames.school.rawValue
    var method: HTTPMethod = .get
    var headers: [String : String]? = ["X-App-Token" : Constants.appToken, "Content-Type" : "application/json"]
    var queryParams: [String : String]? = ["$select" : "dbn, school_name, school_email, website, primary_address_line_1, city, zip, state_code"]
    var body: [String : Any]?
}

class SchoolsRequestAPI: SchoolsRequestAPIProtocol {

    static func getSchools() -> AnyPublisher<[SchoolModel], NetworkError> {
        return APIClient().dispatch(SchoolsRequest())
    }
}
