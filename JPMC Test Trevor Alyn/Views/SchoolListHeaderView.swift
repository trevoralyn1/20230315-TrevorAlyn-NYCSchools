//
//  SchoolListHeaderView.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/15/23.
//

import SwiftUI

struct SchoolListHeaderView: View {
    var body: some View {
        VStack(alignment: .center) {
            Text(LocalizedStringKey("newYorkDepartmentOfEducation"))
                .font(.subheadline)
                .padding(.top, 20)
            Text(LocalizedStringKey("2017HighSchoolDirectory"))
                .font(.title)
            Text(LocalizedStringKey("poweredByNYCOpenData"))
                .font(.footnote)
                .padding(.bottom, 10)
        }
        .padding(.horizontal, 20)
        .frame(maxWidth: .infinity)
        .background(Color.blue.opacity(0.1))
    }
}
