//
//  SchoolItemView.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import SwiftUI

struct SchoolItemView: View {

    var viewModel: SchoolItemViewModel

    var body: some View {
        VStack(alignment: .leading) {
            Text(viewModel.schoolName)
                .font(.headline)
            Text(viewModel.schoolAddressLine1)
            if let city = viewModel.schoolCity, let state = viewModel.schoolStateCode, let zip = viewModel.schoolZip {
                Text("\(city), \(state) \(zip)")
            }
        }
    }
}
