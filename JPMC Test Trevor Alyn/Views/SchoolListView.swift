//
//  SchoolListView.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/15/23.
//

import SwiftUI

struct SchoolListView: View {
    
    @ObservedObject var viewModel = SchoolListViewModel()
    
    var body: some View {
        ZStack {
            NavigationView {
                VStack(spacing: 0) {
                    SchoolListHeaderView()
                    List(viewModel.schoolList) { schoolModel in
                        NavigationLink(destination: SchoolInfoView(viewModel: SchoolInfoViewModel(schoolModel: schoolModel))) {
                            SchoolItemView(viewModel: SchoolItemViewModel(schoolModel: schoolModel))
                        }
                    }
                }
                .navigationBarHidden(true)
            }
            if viewModel.isLoading {
                ProgressView()
            }
        }
        .alert(isPresented: $viewModel.didReceiveError) {
            Alert(title: Text(LocalizedStringKey("error.fetchingResults.headline")),
                  message: Text(LocalizedStringKey("error.fetchingResults.details")),
                  dismissButton: .default(Text(LocalizedStringKey("ok"))))
        }
    }
}
