//
//  SchoolSATInfoItemView.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import SwiftUI
import MessageUI

struct SchoolInfoView: View {
    
    @ObservedObject var viewModel: SchoolInfoViewModel
    
    var body: some View {
        ZStack {
            VStack(alignment: .center) {
                Text(String(localized: "schoolInformation").uppercased())
                    .font(.headline)
                    .padding(.bottom, 20)

                schoolAddressView
                
                schoolInfoView
                
                satScoreInfoView

                Spacer()
            }
            .padding(.horizontal, 20)

            if viewModel.isLoading {
                ProgressView()
            }
        }
        .onAppear {
            viewModel.fetchSchoolSATInfo()
        }
        .alert(isPresented: $viewModel.didReceiveError) {
            Alert(title: Text(LocalizedStringKey("error.fetchingResults.headline")),
                  message: Text(LocalizedStringKey("error.fetchingResults.details")),
                  dismissButton: .default(Text(LocalizedStringKey("ok"))))
        }
    }
    
    var schoolAddressView: some View {
        VStack(alignment: .center) {
            Text(viewModel.schoolName)
                .multilineTextAlignment(.center)
                .font(.title2)
            
            // Don't show the address unless we have all of its parts
            if let schoolAddressLine1 = viewModel.schoolAddressLine1,
               let schoolCity = viewModel.schoolCity,
               let schoolZip = viewModel.schoolZip,
               let schoolStateCode = viewModel.schoolStateCode {
                Text(schoolAddressLine1)
                    .font(.body)
                    .padding(.horizontal, 10)
                Text("\(schoolCity), \(schoolStateCode) \(schoolZip)")
                    .font(.body)
                    .padding(.horizontal, 10)
                    .padding(.bottom, 20)
            }
        }
    }
    
    var schoolInfoView: some View {
        VStack(alignment: .center) {

            Button("Phone: " + viewModel.schoolPhoneNumber) {
                if let url = URL(string: "tel://\(viewModel.schoolPhoneNumber)"), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }
            .padding(.bottom, 10)

            Button("Email: " + viewModel.schoolEmail) {
                EmailHelper.shared.sendEmail(subject: "", body: "", to: viewModel.schoolEmail)
            }
            .padding(.bottom, 10)

            if let schoolURL = viewModel.schoolWebsite {
                Button("Website: " + schoolURL.absoluteString) {
                    if UIApplication.shared.canOpenURL(schoolURL) {
                        UIApplication.shared.open(schoolURL)
                    }
                }
            }
        }
    }
    
    var satScoreInfoView: some View {
        VStack(alignment: .leading) {
            Text("SAT Score Information")
                .font(.title3)
                .padding(.top, 20)
                .padding(.bottom, 5)
            Text(LocalizedStringKey("numberOfTestTakers\(viewModel.numberOfTestTakers)"))
                .font(.body)
                .padding(.horizontal, 10)
                .padding(.bottom, 5)
            Text(LocalizedStringKey("criticalReadingAverageScore\(viewModel.criticalReadingAvgScore)"))
                .font(.body)
                .padding(.horizontal, 10)
                .padding(.bottom, 5)
            Text(LocalizedStringKey("mathAverageScore\(viewModel.mathAvgScore)"))
                .font(.body)
                .padding(.horizontal, 10)
                .padding(.bottom, 5)
            Text(LocalizedStringKey("writingAverageScore\(viewModel.writingAvgScore)"))
                .font(.body)
                .padding(.horizontal, 10)
        }
    }
}
