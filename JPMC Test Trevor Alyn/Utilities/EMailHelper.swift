//
//  EMailHelper.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/15/23.
//

import Foundation
import MessageUI

// Reference: https://thinkdiff.net/how-to-send-email-in-swiftui-5a9047e3442f

class EmailHelper: NSObject, MFMailComposeViewControllerDelegate {
    public static let shared = EmailHelper()
    
    func sendEmail(subject:String, body:String, to:String){
        guard MFMailComposeViewController.canSendMail() else { return }
        
        let picker = MFMailComposeViewController()
        picker.setSubject(subject)
        picker.setMessageBody(body, isHTML: true)
        picker.setToRecipients([to])
        picker.mailComposeDelegate = self
        EmailHelper.getRootViewController()?.present(picker, animated: true, completion: nil)
    }
    
    static func getRootViewController() -> UIViewController? {
        UIApplication.shared.visibleViewController
    }
}
