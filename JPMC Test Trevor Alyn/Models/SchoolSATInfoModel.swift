//
//  SchoolSATInfoModel.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation

public struct SchoolSATInfoModel: Decodable {
    
    public let id: String
    public let schoolName: String?
    public let numberOfTestTakers: String?
    public let criticalReadingAvgScore: String?
    public let mathAvgScore: String?
    public let writingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numberOfTestTakers = "num_of_sat_test_takers"
        case criticalReadingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .dbn)
        self.schoolName = try container.decodeIfPresent(String.self, forKey: .schoolName)
        self.numberOfTestTakers = try container.decodeIfPresent(String.self, forKey: .numberOfTestTakers)
        self.criticalReadingAvgScore = try container.decodeIfPresent(String.self, forKey: .criticalReadingAvgScore)
        self.mathAvgScore = try container.decodeIfPresent(String.self, forKey: .mathAvgScore)
        self.writingAvgScore = try container.decodeIfPresent(String.self, forKey: .writingAvgScore)
    }
}
