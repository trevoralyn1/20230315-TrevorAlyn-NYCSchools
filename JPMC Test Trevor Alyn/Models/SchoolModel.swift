//
//  SchoolModel.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation

public struct SchoolModel: Decodable, Identifiable {
    
    public let id: String
    public let schoolName: String?
    public let phoneNumber: String?
    public let schoolEmail: String?
    public let website: URL?
    public let primaryAddressLine1: String?
    public let city: String?
    public let zip: String?
    public let stateCode: String?
    
    public enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case primaryAddressLine1 = "primary_address_line_1"
        case city
        case zip
        case stateCode = "state_code"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .dbn)
        self.schoolName = try container.decodeIfPresent(String.self, forKey: .schoolName)
        self.phoneNumber = try container.decodeIfPresent(String.self, forKey: .phoneNumber)
        self.schoolEmail = try container.decodeIfPresent(String.self, forKey: .schoolEmail)
        if let urlString = try container.decodeIfPresent(String.self, forKey: .website),
           let url = URL(string: "https://" + urlString) {
            self.website = url
        } else {
            self.website = nil
        }
        self.primaryAddressLine1 = try container.decodeIfPresent(String.self, forKey: .primaryAddressLine1)
        self.city = try container.decodeIfPresent(String.self, forKey: .city)
        self.zip = try container.decodeIfPresent(String.self, forKey: .zip)
        self.stateCode = try container.decodeIfPresent(String.self, forKey: .stateCode)
    }
    
    // Manual constructor to enable unit testing
    public init(id: String,
                schoolName: String,
                phoneNumber: String,
                schoolEmail: String,
                website: URL?,
                primaryAddressLine1: String?,
                city: String?,
                zip: String?,
                stateCode: String?) {
        self.id = id
        self.schoolName = schoolName
        self.phoneNumber = phoneNumber
        self.schoolEmail = schoolEmail
        self.website = website
        self.primaryAddressLine1 = primaryAddressLine1
        self.city = city
        self.zip = zip
        self.stateCode = stateCode
    }
}
