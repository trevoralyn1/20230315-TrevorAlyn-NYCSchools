//
//  JPMC_Test_Trevor_AlynApp.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import SwiftUI

@main
struct JPMC_Test_Trevor_AlynApp: App {

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
