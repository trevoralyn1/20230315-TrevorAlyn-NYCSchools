//
//  Constants.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation

struct Constants {
    static let basePath = "https://data.cityofnewyork.us/resource/"
    static let appToken = "z7rdKdnGmGeIgoB90xKqMO88P"
}

enum JSONFileNames: String {
    case school = "s3k6-pzi2.json"
    case satScores = "f9bf-2cp4.json"
}
