//
//  String+Extensions.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation

extension String {
    
    private func stringFromHttpParameters(params: [String: String]) -> String {
        let parameterArray = params.map { (key, value) -> String in
            guard let percentEscapedKey = key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
                  let percentEscapedValue = value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
                return ""
            }
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        return parameterArray.joined(separator: "&")
    }
}
