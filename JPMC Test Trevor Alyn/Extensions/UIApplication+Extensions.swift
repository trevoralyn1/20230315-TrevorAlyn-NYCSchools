//
//  UIApplication+Extensions.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/15/23.
//

import SwiftUI

// This provides access to the visible viewController, which we can't do in a pure SwiftUI app
// because there is no longer a SceneDelegate.
// Reference: https://swiftuirecipes.com/blog/getting-key-window-and-top-root-view-controller-in-swiftui

// Accessing other functionality that used to be in the SceneDelegate:
// https://betterprogramming.pub/say-goodbye-to-scenedelegate-in-swiftui-444173b23015

extension UIApplication {
    var currentKeyWindow: UIWindow? {
        UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .map { $0 as? UIWindowScene }
            .compactMap { $0 }
            .first?.windows
            .filter { $0.isKeyWindow }
            .first
    }
    
    var visibleViewController: UIViewController? {
        currentKeyWindow?.windowScene?.keyWindow?.rootViewController
    }
}
