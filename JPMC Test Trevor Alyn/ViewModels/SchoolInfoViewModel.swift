//
//  SchoolInfoViewModel.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation
import Combine

public class SchoolInfoViewModel: ObservableObject {
    
    var schoolID: String
    var schoolName = ""
    var schoolAddressLine1: String?
    var schoolCity: String?
    var schoolZip: String?
    var schoolStateCode: String?
    var schoolPhoneNumber: String
    var schoolEmail: String
    var schoolWebsite: URL?
    var numberOfTestTakers = ""
    var criticalReadingAvgScore = ""
    var mathAvgScore = ""
    var writingAvgScore = ""

    @Published var isLoading = false
    @Published var didReceiveError = false

    private var schoolSATInfoModel: SchoolSATInfoModel?
    private var cancellables = Set<AnyCancellable>()
    private var error: Error?

    public init(schoolModel: SchoolModel) {
        self.schoolID = schoolModel.id
        self.schoolName = schoolModel.schoolName ?? String(localized: "schoolNameUnavailable")
        self.schoolAddressLine1 = schoolModel.primaryAddressLine1
        self.schoolCity = schoolModel.city
        self.schoolZip = schoolModel.zip
        self.schoolStateCode = schoolModel.stateCode
        self.schoolPhoneNumber = schoolModel.phoneNumber ?? String(localized: "unavailable")
        self.schoolEmail = schoolModel.schoolEmail ?? String(localized: "unavailable")
        self.schoolWebsite = schoolModel.website
    }
    
    func fetchSchoolSATInfo() {
        isLoading = true
        SchoolSATInfoRequestAPI.getSATInfo(schoolID: schoolID)
            .receive(on: DispatchQueue.main, options: nil)
            .sink { [weak self] response in
                print(response)
                self?.isLoading = false
                switch response {
                case .finished:
                    break
                case .failure(let error):
                    self?.didReceiveError = true
                    // TODO: Customize error alert based on error type
                    self?.error = error
                }
            } receiveValue: { [weak self] schoolSATInfoResponseModels in
                
                // This returns an array, but here we're fetching by school ID, so there should only be one item.
                // Note that the array is sometimes empty.
                if schoolSATInfoResponseModels.count > 0, let schoolSATInfoResponseModel = schoolSATInfoResponseModels.first {
                    self?.schoolSATInfoModel = schoolSATInfoResponseModel
                }
                self?.populateUI()
            }
            .store(in: &cancellables)
    }
    
    private func populateUI() {
        numberOfTestTakers = schoolSATInfoModel?.numberOfTestTakers ?? String(localized: "unavailable")
        criticalReadingAvgScore = schoolSATInfoModel?.criticalReadingAvgScore ?? String(localized: "unavailable")
        mathAvgScore = schoolSATInfoModel?.mathAvgScore ?? String(localized: "unavailable")
        writingAvgScore = schoolSATInfoModel?.writingAvgScore ?? String(localized: "unavailable")
    }
}
