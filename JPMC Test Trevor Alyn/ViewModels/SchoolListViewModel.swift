//
//  SchoolListViewModel.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import Foundation
import Combine

public class SchoolListViewModel: ObservableObject {
    
    var schoolList = [SchoolModel]()
    
    @Published var isLoading = false
    @Published var didReceiveError = false

    private var cancellables = Set<AnyCancellable>()
    private var error: Error?
    
    init() {
        fetchSchools()
    }
    
    private func fetchSchools() {
        isLoading = true
        SchoolsRequestAPI.getSchools()
            .receive(on: DispatchQueue.main, options: nil)
            .sink { [weak self] response in
                self?.isLoading = false
                switch response {
                case .finished:
                    break
                case .failure(let error):
                    self?.didReceiveError = true
                    // TODO: Customize error alert based on error type
                    self?.error = error
                }
            } receiveValue: { [weak self] schoolModels in
                self?.schoolList = schoolModels
            }
            .store(in: &cancellables)
    }
}
