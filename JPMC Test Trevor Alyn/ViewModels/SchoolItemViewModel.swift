//
//  SchoolItemViewModel.swift
//  JPMC Test Trevor Alyn
//
//  Created by Trevor Alyn on 3/14/23.
//

import SwiftUI

public class SchoolItemViewModel {

    let schoolModel: SchoolModel

    var schoolID: String
    var schoolName: String
    var schoolPhoneNumber: String
    var schoolEmail: String
    var schoolWebsite: URL?
    var schoolAddressLine1: String
    var schoolCity: String?
    var schoolZip: String?
    var schoolStateCode: String?
    
    init(schoolModel: SchoolModel) {
        self.schoolModel = schoolModel
        self.schoolID = schoolModel.id
        self.schoolName = schoolModel.schoolName ?? String(localized: "schoolNameUnavailable")
        self.schoolPhoneNumber = schoolModel.phoneNumber ?? String(localized: "unavailable")
        self.schoolEmail = schoolModel.schoolEmail ?? String(localized: "unavailable")
        self.schoolWebsite = schoolModel.website
        self.schoolAddressLine1 = schoolModel.primaryAddressLine1 ?? String(localized: "addressUnavailable")
        self.schoolCity = schoolModel.city
        self.schoolZip = schoolModel.zip
        self.schoolStateCode = schoolModel.stateCode
    }
}
