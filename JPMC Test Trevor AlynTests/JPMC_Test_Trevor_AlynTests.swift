//
//  JPMC_Test_Trevor_AlynTests.swift
//  JPMC Test Trevor AlynTests
//
//  Created by Trevor Alyn on 3/14/23.
//

import XCTest
import Combine

@testable import JPMC_Test_Trevor_Alyn

class MockSchoolsRequestAPI: SchoolsRequestAPIProtocol {
    
    static var fetchSchoolsResult: AnyPublisher<[JPMC_Test_Trevor_Alyn.SchoolModel], JPMC_Test_Trevor_Alyn.NetworkError>!
    
    static func getSchools() -> AnyPublisher<[JPMC_Test_Trevor_Alyn.SchoolModel], JPMC_Test_Trevor_Alyn.NetworkError> {
        return fetchSchoolsResult
    }
}

final class JPMC_Test_Trevor_AlynTests: XCTestCase {
    
    private var mockSchoolsRequestAPI: MockSchoolsRequestAPI!
    private var cancellables: Set<AnyCancellable>!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        mockSchoolsRequestAPI = MockSchoolsRequestAPI()
        cancellables = []
    }
    
    override func tearDownWithError() throws {
        mockSchoolsRequestAPI = nil
        cancellables = nil
    }
    
    func testFetchSchools() {
        let schoolsToTest = [SchoolModel(id: "", schoolName: "", phoneNumber: "", schoolEmail: "", website: nil, primaryAddressLine1: "", city: "", zip: "", stateCode: "")]
        let expectation = XCTestExpectation(description: "Schools fetched")
        
        MockSchoolsRequestAPI.fetchSchoolsResult = Result.success(schoolsToTest).publisher.eraseToAnyPublisher()
        MockSchoolsRequestAPI.getSchools()
            .sink(receiveCompletion: { response in
                switch response {
                case .finished:
                    break
                case .failure:
                    XCTFail("Failed to receive school list")
                }
            }, receiveValue: { schoolModels in
                XCTAssertEqual(schoolModels.count, 1)
                expectation.fulfill()
            })
            .store(in: &cancellables)
    }
}
